# Outlook Python Scripts
Collection of some of my scripts to work with outlook using python win32com library.

- gcalendar_sync.py will sync my outlook calendar with google calendar. Only the time and subject will be synced, but not the body.
- outlook_attachment.py will download pdf attachments from my mail which match a particular search phrase. Made this to download all my salary slips.
